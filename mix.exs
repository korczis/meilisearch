defmodule Meilisearch.MixProject do
  use Mix.Project

  def project do
    [
      app: :meilisearch,
      version: "0.0.1",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        "coveralls.cobertura": :test,
        "test.watch": :test
      ],

      # Docs
      name: "Meilisearch",
      source_url: "https://gitlab.com/korczis/meilisearch",
      homepage_url: "http://gitlab.com/korczis/gitlab",
      docs: [
        # The main page in the docs
        main: "Meilisearch",
        # logo: "path/to/logo.png",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Meilisearch.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 1.4", only: [:dev, :test], runtime: false},
      {:benchee, "~> 1.3"},
      {:benchee_html, "~> 1.0"},
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.18", only: :test},
      {:ex_doc, "~> 0.31", only: :dev, runtime: false},
      {:git_ops, "~> 2.6.1", only: [:dev]},
      {:jason, "~> 1.4"},
      {:mix_test_watch, "~> 1.2", only: [:dev, :test], runtime: false},
      {:nimble_csv, "~> 1.2"},
      {:poison, "~> 5.0"},
      {:tesla, "~> 1.9"},
      {:gen_stage, "~> 1.2"},
      {:flop, "~> 0.26"},
    ]
  end
end
