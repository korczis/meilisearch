defmodule Meilisearch.Consumer do
  use GenStage

  def start_link() do
    GenStage.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    # Subscribe to the producer with demand control
    {:consumer, :ok, subscribe_to: [Meilisearch.Producer]}
  end

  def handle_events(events, _from, state) do
    # Process each event (batch)
    Enum.each(events, fn event -> process_event(event) end)
    {:noreply, [], state}
  end

  defp process_event(event) do
    IO.inspect(event, label: "Processed Event")
    # Here, you would handle the actual query result processing
  end
end