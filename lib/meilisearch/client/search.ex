#defmodule Meilisearch.Search do
#  @moduledoc """
#  Meilisearch exposes 2 routes to perform searches:
#
#  A POST route: this is the preferred route when using API authentication, as it allows [preflight request](https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request) caching and better performances
#  A GET route: the usage of this route is discouraged, unless you have good reason to do otherwise (specific caching abilities for example)
#  You may find exhaustive descriptions of the parameters accepted by the two routes [at the end of this article](https://www.meilisearch.com/docs/reference/api/search#search-parameters).
#  """
#  defmacro __using__(_opts \\ []) do
#    quote do
#      def search_documents(uid, query) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/search")
#        |> URI.append_query(URI.encode_query(q: query))
#        |> URI.to_string()
#        |> get()
#      end
#    end
#  end
#end
