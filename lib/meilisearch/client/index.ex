#defmodule Meilisearch.Index do
#  @moduledoc """
#  Indexes
#
#  The `/indexes` route allows you to create, manage, and delete your indexes.
#  """
#
#  defmacro __using__(_opts \\ []) do
#    quote do
#      @doc """
#        List all indexes. Results can be paginated by using the offset and limit query parameters.
#      """
#      def list_indexes(offset \\ 0, limit \\ 20) do
#        base_url()
#        |> URI.append_path("/indexes")
#        |> URI.append_query(URI.encode_query(offset: offset, limit: limit))
#        |> URI.to_string()
#        |> get()
#      end
#
#      @doc """
#        Get information about an index.
#      """
#      def get_index(uid) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}")
#        |> URI.to_string()
#        |> get()
#      end
#
#      @doc """
#        Create an index.
#      """
#      def create_index(uid, primary_key) do
#        base_url()
#        |> URI.append_path("/indexes")
#        |> URI.to_string()
#        |> post(%{
#          uid: uid,
#          primaryKey: primary_key
#        })
#      end
#
#      @doc """
#        Update an index's primary key. You can freely update the primary key of an index as long as it contains no documents.
#
#      To change the primary key of an index that already contains documents, you must first delete all documents in that index. You may then change the primary key and index your dataset again.
#      """
#      def update_index(uid, primary_key) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}")
#        |> URI.to_string()
#        |> patch(%{
#          primaryKey: primary_key
#        })
#      end
#
#      @doc """
#        Delete an index.
#      """
#      def delete_index(uid) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}")
#        |> URI.to_string()
#        |> delete()
#      end
#
#      @doc """
#        Swap the documents, settings, and task history of two or more indexes. You can only swap indexes in pairs. However, a single request can swap as many index pairs as you wish.
#
#      Swapping indexes is an atomic transaction: either all indexes are successfully swapped, or none are.
#
#      Swapping indexA and indexB will also replace every mention of indexA by indexB and vice-versa in the task history. enqueued tasks are left unmodified.
#
#      [To learn more about index swapping, refer to this short guide.](https://www.meilisearch.com/docs/learn/core_concepts/indexes#swapping-indexes)
#      """
#      def swap_indexes(indexes) do
#        base_url()
#        |> URI.append_path("/swap-indexes")
#        |> URI.to_string()
#        |> post(%{
#          indexes: indexes
#        })
#      end
#    end
#  end
#end
