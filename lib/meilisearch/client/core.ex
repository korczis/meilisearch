#defmodule Meilisearch.Core do
#  @moduledoc """
#  Core Client used by other sub-modulesw
#  """
#
#  defmacro __using__(_opts \\ []) do
#    quote do
#      use Tesla
#
#      @base_url "http://localhost:7700"
#
#      plug Tesla.Middleware.BaseUrl, @base_url
#      plug(Tesla.Middleware.JSON,
#        encode_content_type: "application/json",
#        engine_opts: [keys: :atoms]
#      )
#      # plug(Tesla.Middleware.Compression, format: "gzip")
#      plug(Tesla.Middleware.Headers, [{"content-type", "application/json"}])
#
#      # Add Bearer token authentication
#      # plug Tesla.Middleware.Headers, fn -> [{"Authorization", "Bearer " <> get_bearer_token()}] end
#
#       plug Tesla.Middleware.BearerAuth, token: System.get_env("MEILI_MASTER_KEY")
#
#      # Function to get the Bearer token (you can customize this)
##      defp get_bearer_token do
##        # Replace this with how you retrieve your token, e.g., from env, a database, or an API call
##        System.get_env("MEILI_MASTER_KEY")
##      end
#
#      def base_url(), do: System.get_env("MEILI_API_ADDR", @base_url) |> URI.parse()
#    end
#  end
#end
