#defmodule Meilisearch.Document do
#  @moduledoc """
#  Documents
#  The `/documents` route allows you to create, manage, and delete documents.
#
#  [Learn more about documents.](https://www.meilisearch.com/docs/learn/core_concepts/documents)
#  """
#
#  defmacro __using__(_opts \\ []) do
#    quote do
#      def get_documents(uid, offset \\ 0, limit \\ 20, fields \\ nil, filter \\ nil) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/documents/fetch")
#        |> URI.to_string()
#        |> post(%{
#          offset: offset,
#          limit: limit,
#          fields: fields,
#          filter: filter
#        })
#      end
#
#      def get_document(uid, id) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/documents/#{id}")
#        |> URI.to_string()
#        |> post(%{})
#      end
#
#      def add_or_replace_documents(uid, docs, primary_key \\ nil)
#          when is_list(docs) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/documents")
#        |> URI.append_query(
#          URI.encode_query(primaryKey: primary_key)
#        )
#        |> URI.to_string()
#        |> post(docs)
#      end
#
#      def add_or_replace_document(index, doc, primary_key \\ nil),
#        do: add_or_replace_documents(index, [doc], primary_key)
#
#      def add_or_update_documents(uid, docs, primary_key \\ nil)
#          when is_list(docs) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/documents")
#        |> URI.append_query(
#          URI.encode_query(primaryKey: primary_key)
#        )
#        |> URI.to_string()
#        |> post(docs)
#      end
#
#      def add_or_update_document(index, doc, primary_key \\ nil),
#        do: add_or_update_documents(index, [doc], primary_key)
#
#      def delete_all_documents(uid) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/documents")
#        |> URI.to_string()
#        |> delete()
#      end
#
#      def delete_document(uid, id) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/documents/#{id}")
#        |> URI.to_string()
#        |> delete()
#      end
#
#      #      @dialyzer {:nowarn_function, delete_documents_by_filter: 2}
#      #      def delete_documents_by_filter(uid, filter) do
#      #        base_url()
#      #        |> URI.append_path("/indexes/#{uid}/documents/delete")
#      #        |> URI.to_string()
#      #        |> delete([
#      #          filter: filter
#      #        ])
#      #      end
#
#      def delete_documents_by_batch(uid, ids) do
#        base_url()
#        |> URI.append_path("/indexes/#{uid}/documents/delete-batch")
#        |> URI.to_string()
#        |> delete(ids)
#      end
#    end
#  end
#end
