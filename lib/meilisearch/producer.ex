defmodule Meilisearch.Producer do
  use GenStage

  def start_link(initial_state) do
    GenStage.start_link(__MODULE__, initial_state, name: __MODULE__)
  end

  def init(state) do
    {:producer, state}
  end

  def handle_demand(demand, state) when demand > 0 do
    # Let's assume `fetch_batch/1` retrieves a batch from Meilisearch
    batch = fetch_batch(state)
    {:noreply, batch, state}
  end

  defp fetch_batch(state) do
    # Retrieve a batch of data from Meilisearch or any data source
    # Here we simulate batch data; in production, you'd call the real data source.
    Enum.map(1..state.batch_size, fn i -> %{id: i, data: "Item #{i}"} end)
  end
end

