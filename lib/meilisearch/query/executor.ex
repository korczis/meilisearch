defmodule Meilisearch.Query.Executor do
  @moduledoc """
  The `Meilisearch.Query.Executor` module is responsible for executing a query against the Meilisearch client.
  It prepares the necessary parameters and sends the request to the Meilisearch server. It handles the query execution
  including filters, sorts, and pagination settings.

  ## Example

      iex> query = %Meilisearch.Query{
      ...>   index: "products",
      ...>   q: "laptop",
      ...>   filters: [%{field: "price", op: ">", value: 100}],
      ...>   sort: [{"price", "desc"}],
      ...>   limit: 10,
      ...>   offset: 0
      ...> }
      ...> 
      ...> Meilisearch.Query.Executor.execute(query)
      {:ok, %{"hits" => [%{"name" => "Laptop A", "price" => 120}]}}

  """

  require Logger

  @doc """
  Executes a Meilisearch query.

  This function takes a `%Meilisearch.Query{}` struct, builds the appropriate parameters, and sends them to the Meilisearch client
  for execution. It returns the results of the query as a tuple of `{:ok, results}`.

  ## Parameters
  - `query`: A `%Meilisearch.Query{}` struct containing the query parameters.

  ## Returns
  - `{:ok, results}`: The search results returned by Meilisearch.

  ## Example

      iex> query = %Meilisearch.Query{index: "products", q: "laptop", limit: 10}
      ...> Meilisearch.Query.Executor.execute(query)
      {:ok, %{"hits" => [%{"name" => "Laptop A", "price" => 120}]}}

  """
  def execute(%Meilisearch.Query{} = query, opts \\ []) do
    payload = build_payload(query)

    if Keyword.get(opts, :log, false) do
      Logger.debug("Executing query: #{inspect(query)}, payload: #{inspect(payload)}")
    end

    with {:ok, results} <- Meilisearch.Search.search(query.index, payload) do
      {:ok, results}
    end
  end

  @doc """
  Builds the query parameters required for a Meilisearch search.

  This function prepares the search parameters by adding optional fields like search terms, filters, sorting, and pagination.

  ## Parameters
  - `query`: The `%Meilisearch.Query{}` struct containing the query parameters.

  ## Returns
  - `Map`: The query parameters in the form of a map.

  ## Example

      iex> query = %Meilisearch.Query{
      ...>   q: "laptop",
      ...>   filters: [%{field: "price", op: ">", value: 100}],
      ...>   sort: ["price:asc"],
      ...>   limit: 10,
      ...>   offset: 0
      ...> }
      ...> 
      ...> Meilisearch.Query.Executor.build_payload(query)
      %{q: "laptop", filter: "price > 100", sort: "price:asc", limit: 10, offset: 0}

  """
  def build_payload(query) do
    %{}
    |> maybe_add_search(query.q)
    |> maybe_add_filters(query.filters)
    |> maybe_add_sort(query.sort)
    |> Map.put(:limit, query.limit)
    |> Map.put(:offset, query.offset)
  end

  @doc """
  Conditionally adds the search term to the query parameters.

  ## Parameters
  - `params`: The current query parameters.
  - `q`: The search term string to include in the query.

  ## Returns
  - `Map`: The updated query parameters.

  ## Example

      iex> Meilisearch.Query.Executor.maybe_add_search(%{}, "laptop")
      %{q: "laptop"}

      iex> Meilisearch.Query.Executor.maybe_add_search(%{}, nil)
      %{}
  """
  defp maybe_add_search(params, nil), do: params
  defp maybe_add_search(params, q), do: Map.put(params, :q, q)

  @doc """
  Conditionally adds the filters to the query parameters.

  ## Parameters
  - `params`: The current query parameters.
  - `filters`: The list of filters to apply to the query.

  ## Returns
  - `Map`: The updated query parameters with the added filters.

  ## Example

      iex> Meilisearch.Query.Executor.maybe_add_filters(%{}, [%{field: "price", op: ">", value: 100}])
      %{filter: "price > 100"}

      iex> Meilisearch.Query.Executor.maybe_add_filters(%{}, [])
      %{}
  """
  defp maybe_add_filters(params, []), do: params

  defp maybe_add_filters(params, filters) do
    filter_string = Enum.map_join(filters, " AND ", &build_filter_field/1)
    Map.put(params, :filter, filter_string)
  end

  @doc """
  Conditionally adds sorting criteria to the query parameters.

  ## Parameters
  - `params`: The current query parameters.
  - `sorts`: The list of sorting criteria to apply to the query.

  ## Returns
  - `Map`: The updated query parameters with the added sorting criteria.

  ## Example

      iex> Meilisearch.Query.Executor.maybe_add_sort(%{}, ["price:asc"])
      %{sort: "price:asc"}

      iex> Meilisearch.Query.Executor.maybe_add_sort(%{}, [])
      %{}
  """
  defp maybe_add_sort(params, []), do: params

  defp maybe_add_sort(params, sorts) do
    sorts = Enum.map(sorts, &build_sort_field/1)
    Map.put(params, :sort, sorts)
  end

  @doc """
  Builds the string representation of a filter.

  ## Parameters
  - `filter`: A map containing the filter parameters.

  ## Returns
  - `String`: The filter string.

  ## Example

      iex> Meilisearch.Query.Executor.build_filter_field(%{field: "price", op: ">", value: 100})
      "price > 100"

      iex> Meilisearch.Query.Executor.build_filter_field(%{
      ...>   field: "category",
      ...>   op: "IN",
      ...>   value: ["electronics", "furniture"]
      ...> })
      "category IN ['electronics', 'furniture']"

  """
  defp build_filter_field(%{field: field, op: "EXISTS"}) do
    "#{field} EXISTS"
  end

  defp build_filter_field(%{field: field, op: "IN", value: value}) do
    "#{field} IN [#{Enum.map_join(value, ",", &"'#{&1}'")}]"
  end

  defp build_filter_field(%{field: field, op: op, value: value}) do
    "#{field} #{op} #{format_value(value)}"
  end

  @doc """
  Builds the string representation of a sorting condition.

  ## Parameters
  - `sort`: A tuple containing the field and sorting direction.

  ## Returns
  - `String`: The sorting string.

  ## Example

      iex> Meilisearch.Query.Executor.build_sort_field({"price", "asc"})
      "price:asc"

      iex> Meilisearch.Query.Executor.build_sort_field({"name", "desc"})
      "name:desc"

  """
  defp build_sort_field({field, direction}) do
    "#{field}:#{direction}"
  end

  @doc """
  Formats a value for use in a query.

  ## Parameters
  - `value`: The value to format.

  ## Returns
  - `String`: The formatted value.

  ## Example

      iex> Meilisearch.Query.Executor.format_value("laptop")
      "'laptop'"

      iex> Meilisearch.Query.Executor.format_value(100)
      "100"

  """
  defp format_value(value) when is_binary(value), do: "'#{escape_value(value)}'"
  defp format_value(value), do: to_string(value)

  @doc """
  Escapes special characters in a string value.

  ## Parameters
  - `value`: The value to escape.

  ## Returns
  - `String`: The escaped string.

  ## Example

      iex> Meilisearch.Query.Executor.escape_value("O'Reilly")
      "O\\'Reilly"

  """
  defp escape_value(value) do
    String.replace(value, ["'", "\\"], &"\\#{&1}")
  end
end
