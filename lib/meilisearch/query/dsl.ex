defmodule Meilisearch.Query.DSL do
  @moduledoc """
  Provides a domain-specific language (DSL) for building queries in Meilisearch.

  This module provides a set of functions and macros that allow you to construct queries in a readable way using a pipeline.

  ## Example Usage

  ```elixir
  query =
    from("products")
    |> where(fn p -> exists(p.price) end)
    |> search("Laptop")
    |> order_by(["price"])
    |> limit(10)
    |> offset(0)

  Meilisearch.Query.Executor.execute(query)
  ```

  ## Functions and Macros

  - `from/1`: Specifies the index to search.
  - `where/2`: Adds filter conditions to the query.
  - `search/2`: Adds a search term to the query.
  - `order_by/2`: Defines the sorting order of results.
  - `limit/2`: Sets the limit for the number of results.
  - `offset/2`: Sets the offset for pagination.
  """

  alias Meilisearch.Query
  alias Meilisearch.Query.Executor

  @doc """
  Initializes a new query for a given index.

  ## Parameters
  - `index` - A string representing the index to search against.

  ## Example

      iex> query = from("products")
      ...> query.index
      "products"

  """
  def from(index), do: Query.from(index)

  @doc """
  Adds a limit to the query, specifying the maximum number of results to return.

  ## Parameters
  - `query` - The query struct to modify.
  - `limit` - The maximum number of results to return (integer greater than 0).

  ## Example

      iex> query = from("products") |> limit(10)
      ...> query.limit
      10

  """
  def limit(query, limit), do: Query.limit(query, limit)

  @doc """
  Adds an offset to the query, specifying the starting point for results.

  ## Parameters
  - `query` - The query struct to modify.
  - `offset` - The number of results to skip (integer greater than or equal to 0).

  ## Example

      iex> query = from("products") |> offset(5)
      ...> query.offset
      5

  """
  def offset(query, offset), do: Query.offset(query, offset)

  @doc """
  Adds sorting criteria to the query, ordering the results by specified fields.

  ## Parameters
  - `query` - The query struct to modify.
  - `sorts` - A list of fields to sort by (strings or atoms).

  ## Example

      iex> query = from("products") |> order_by(["price", "name"])
      ...> query.sort
      ["price", "name"]

  """
  def order_by(query, sorts), do: Query.order_by(query, sorts)

  @doc """
  Adds a filter condition to the query based on a custom function.

  This macro allows you to add filters like `field == value`, `field > value`, etc.

  ## Parameters
  - `query` - The query struct to modify.
  - `fun` - A function that defines the condition to filter by (e.g., `fn p -> p.price > 100`).

  ## Example

      iex> query = from("products") |> where(fn p -> p.price > 100 end)
      ...> query.filters
      [%{field: "price", op: ">", value: 100}]

  """
  defmacro where(query, fun) do
    quote do
      Query.add_filter(unquote(query), unquote(expand_where(fun)))
    end
  end

  # TODO: Document
  def search(query, search_term), do: Query.search(query, search_term)

  # TODO: Document
  def execute(query), do: Executor.execute(query)

  @doc """
  Expands the given function into a valid Meilisearch query filter.

  ## Parameters
  - `fun` - A function that defines the condition to filter by.

  ## Example

      iex> expand_where(fn p -> p.price > 100 end)
      %{field: "price", op: ">", value: 100}

  """
  defp expand_where({:fn, _, [{:->, _, [[_var], expr]}]}) do
    expand_expr(expr)
  end

  @doc """
  Expands a comparison expression into a filter format.

  ## Parameters
  - `expr` - The comparison expression to convert (e.g., `p.price > 100`).

  ## Example

      iex> expand_expr({:>, _, [p.price, 100]})
      %{field: "price", op: ">", value: 100}

  """
  defp expand_expr({op, _, [left, right]}) when op in [:==, :>, :<, :>=, :<=, :!=] do
    create_filter(left, op, right)
  end

  @doc """
  Expands a `contains` condition into a filter format.

  ## Parameters
  - `left` - The field to check.
  - `right` - The value to search for.

  ## Example

      iex> expand_expr({:contains, _, [p.name, "Laptop"]})
      %{field: "name", op: "CONTAINS", value: "Laptop"}

  """
  defp expand_expr({:contains, _, [left, right]}) do
    create_filter(left, "CONTAINS", right)
  end

  @doc """
  Expands an `exists` condition into a filter format.

  ## Parameters
  - `left` - The field to check for existence.

  ## Example

      iex> expand_expr({:exists, _, [p.price]})
      %{field: "price", op: "EXISTS"}

  """
  defp expand_expr({:exists, _, [left]}) do
    Macro.escape(%{
      field: to_string(extract_field(left)),
      op: "EXISTS"
    })
  end

  @doc """
  Expands an `is_in` condition into a filter format.

  ## Parameters
  - `left` - The field to check.
  - `right` - The list of values to check against.

  ## Example

      iex> expand_expr({:is_in, _, [p.category, ["electronics", "furniture"]]})
      %{field: "category", op: "IN", value: ["electronics", "furniture"]}

  """
  defp expand_expr({:is_in, _, [left, right]}) when is_list(right) do
    create_filter(left, "IN", right)
  end

  @doc """
  Expands a `like` condition into a filter format.

  ## Parameters
  - `left` - The field to check.
  - `right` - The pattern to check against.

  ## Example

      iex> expand_expr({:like, _, [p.name, "Laptop%"]})
      %{field: "name", op: "LIKE", value: "Laptop%"}

  """
  defp expand_expr({:like, _, [left, right]}) do
    create_filter(left, "LIKE", right)
  end

  @doc """
  Creates a filter map for a field and operator.

  ## Parameters
  - `left` - The field to check.
  - `op` - The operator to apply (e.g., `>`, `==`).
  - `right` - The value to compare against.

  ## Example

      iex> create_filter(p.price, ">", 100)
      %{field: "price", op: ">", value: 100}

  """
  defp create_filter(left, op, right) do
    Macro.escape(%{
      field: to_string(extract_field(left)),
      op: op,
      value: right
    })
  end

  @doc """
  Extracts the field name from an Elixir struct.

  ## Parameters
  - `left` - The field reference to extract the name from.

  ## Example

      iex> extract_field(p.price)
      :price

  """
  defp extract_field({{:., _, [{var, _, _}, field]}, _, []}) when is_atom(field), do: field
  defp extract_field({:., _, [{var, _, _}, field]}) when is_atom(field), do: field
end
