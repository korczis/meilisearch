defmodule Meilisearch.Query.API do
  defmacro __using__(_opts) do
    quote do
      import Meilisearch.Query.DSL

      def execute(query) do
        Meilisearch.Query.Executor.execute(query)
      end
    end
  end
end
