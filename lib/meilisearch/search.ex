defmodule Meilisearch.Search do
  @moduledoc """
  Meilisearch exposes 2 routes to perform searches:

  A POST route: this is the preferred route when using API authentication, as it allows [preflight request](https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request) caching and better performances
  A GET route: the usage of this route is discouraged, unless you have good reason to do otherwise (specific caching abilities for example)
  You may find exhaustive descriptions of the parameters accepted by the two routes [at the end of this article](https://www.meilisearch.com/docs/reference/api/search#search-parameters).
  """

  import Meilisearch.Client

  require Logger

  def search(uid, query) when is_binary(uid) and is_map(query) do
    with {:ok, %Tesla.Env{body: body}} <-
           base_url()
           |> URI.append_path("/indexes/#{uid}/search")
           |> URI.to_string()
           |> post(query) do
      {:ok, body}
    end
  end

  def list_items(uid, schema, params \\ %{}) do
    with {:ok, flop} <- Flop.validate(params, for: schema),
         query <- build_meilisearch_query(flop, params),
         {:ok, results} <- search(uid, query) do
      items = transform_results(schema, results)
      meta = build_meta(flop, results)

      {:ok, {items, meta}}
    else
      {:error, _} = error -> error
    end
  end

  defp build_meilisearch_query(flop, params) do
    query = %{
      offset: calculate_offset(flop),
      limit: flop.limit || 20,
      sort: build_sort(flop.order_by, flop.order_directions),
      filter: build_filters(flop.filters)
    }

    query =
      params
      |> Map.delete(:sort)
      |> Map.delete(:filters)
      |> Map.merge(query)

    Logger.debug("Built meilisearch query from flop: #{inspect(flop)}, query: #{inspect(query)}")

    query
  end

  defp calculate_offset(flop) do
    page = flop.page || 1
    limit = flop.limit || 20
    max(0, (page - 1) * limit)
  end

  defp build_sort(nil, _), do: []
  defp build_sort([], _), do: []

  defp build_sort(order_by, directions) do
    order_by
    |> Enum.zip(directions || [])
    |> Enum.map(fn {field, direction} ->
      "#{field}:#{direction || :asc}"
    end)
  end

  defp build_filters(nil), do: nil
  defp build_filters([]), do: nil

  defp build_filters(filters) when is_list(filters) do
    filters
    |> Enum.map(&build_filter/1)
    |> Enum.reject(&is_nil/1)
    |> case do
      [] -> nil
      conditions -> Enum.join(conditions, " AND ")
    end
  end

  defp into_op(op) when is_binary(op), do: String.to_atom!(op)
  defp into_op(op), do: op

  defp build_filter(%{field: field, op: op, value: value}) when not is_nil(value) do
    case into_op(op) do
      :== ->
        "#{field} = #{format_value(value)}"

      :!= ->
        "#{field} != #{format_value(value)}"

      :>= ->
        "#{field} >= #{format_value(value)}"

      :> ->
        "#{field} > #{format_value(value)}"

      :<= ->
        "#{field} <= #{format_value(value)}"

      :< ->
        "#{field} < #{format_value(value)}"

      :in when is_list(value) ->
        values =
          Enum.map_join(value, " OR ", fn v ->
            "#{field} = #{format_value(v)}"
          end)

        "(#{values})"

      :not_in when is_list(value) ->
        values =
          Enum.map_join(value, " AND ", fn v ->
            "#{field} != #{format_value(v)}"
          end)

        "(#{values})"

      :empty ->
        "NOT EXISTS(#{field})"

      :not_empty ->
        "EXISTS(#{field})"

      # Text search operations - these might need adjustment based on your Meilisearch setup
      :contains ->
        "#{field} = '*#{escape_value(value)}*'"

      :not_contains ->
        "#{field} != '*#{escape_value(value)}*'"

      :like ->
        "#{field} = '*#{escape_value(value)}*'"

      :not_like ->
        "#{field} != '*#{escape_value(value)}*'"

      :ilike ->
        "#{field} = '*#{escape_value(value)}*'"

      :not_ilike ->
        "#{field} != '*#{escape_value(value)}*'"

      :like_and when is_list(value) ->
        values =
          Enum.map_join(value, " AND ", fn v ->
            "#{field} = '*#{escape_value(v)}*'"
          end)

        "(#{values})"

      :like_or when is_list(value) ->
        values =
          Enum.map_join(value, " OR ", fn v ->
            "#{field} = '*#{escape_value(v)}*'"
          end)

        "(#{values})"

      :ilike_and when is_list(value) ->
        values =
          Enum.map_join(value, " AND ", fn v ->
            "#{field} = '*#{escape_value(v)}*'"
          end)

        "(#{values})"

      :ilike_or when is_list(value) ->
        values =
          Enum.map_join(value, " OR ", fn v ->
            "#{field} = '*#{escape_value(v)}*'"
          end)

        "(#{values})"

      _ ->
        nil
    end
  end

  defp build_filter(_), do: nil

  defp format_value(value) when is_binary(value), do: "'#{escape_value(value)}'"
  defp format_value(value) when is_number(value), do: value
  defp format_value(value) when is_boolean(value), do: value
  defp format_value(value) when is_nil(value), do: "null"
  defp format_value(_), do: nil

  defp escape_value(value) when is_binary(value) do
    String.replace(value, ["'", "\\"], &"\\#{&1}")
  end

  defp build_meta(flop, results) do
    total_count = results.estimatedTotalHits || 0
    page_size = flop.limit || 20

    %Flop.Meta{
      current_page: flop.page || 1,
      page_size: page_size,
      total_count: total_count,
      total_pages: max(ceil(total_count / page_size), 1)
    }
  end

  defp transform_results(schema, results) do
    results.hits
    |> Enum.map(fn hit ->
      struct(schema, hit)
    end)
  rescue
    _ -> []
  end
end
