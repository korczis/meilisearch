defmodule Meilisearch.Query do
  @moduledoc """
  This module provides the core functionality for building and manipulating queries in Meilisearch.

  It defines a `Query` struct that represents a query object, containing details about the search index, filters, sorting criteria, limits, offsets, and the search term itself.

  ## Query Structure

  The `Query` struct includes the following fields:

    - `:index` - The index name to perform the search on.
    - `:filters` - A list of filters to apply to the query.
    - `:sort` - A list of fields by which to sort the query results.
    - `:limit` - The number of results to return (default is 20).
    - `:offset` - The offset for pagination (default is 0).
    - `:q` - The search term or query string.

  The `Meilisearch.Query` module provides functions for building a query, applying filters, sorting, setting limits and offsets, and assigning a search term.

  ## Examples

      iex> query = Meilisearch.Query.from("products")
      ...> query = Meilisearch.Query.add_filter(query, %{field: "price", op: ">", value: 100})
      ...> query = Meilisearch.Query.search(query, "laptop")
      ...> query = Meilisearch.Query.limit(query, 10)
      ...> query = Meilisearch.Query.offset(query, 0)
      ...> query = Meilisearch.Query.order_by(query, ["price"])

  """

  defstruct [:index, :filters, :sort, :limit, :offset, :q]

  @doc """
  Initializes a new query for the specified index.

  This function creates a `Query` struct with default values for filters, sorting, limit, and offset.

  ## Parameters
    - `index` (string): The name of the index to search on.

  ## Returns
    - `%Meilisearch.Query{}`: A new query struct with default settings.

  ## Example
      iex> Meilisearch.Query.from("products")
      %Meilisearch.Query{index: "products", filters: [], sort: [], limit: 20, offset: 0, q: nil}
  """
  def from(index) when is_binary(index) do
    %__MODULE__{
      index: index,
      filters: [],
      sort: [],
      limit: 20,
      offset: 0,
      q: nil
    }
  end

  @doc """
  Adds a filter to the query.

  This function allows the user to add a filter to the existing query. The filter should be a map containing `field`, `op`, and `value` keys.

  ## Parameters
    - `query` (Meilisearch.Query): The current query struct.
    - `filter` (map): A map representing the filter to add, which must contain `field`, `op` (operation), and `value`.

  ## Returns
    - `%Meilisearch.Query{}`: The updated query struct with the new filter.

  ## Example
      iex> query = Meilisearch.Query.from("products")
      ...> filter = %{field: "price", op: ">", value: 100}
      ...> Meilisearch.Query.add_filter(query, filter)
      %Meilisearch.Query{
        index: "products",
        filters: [%{field: "price", op: ">", value: 100}],
        sort: [],
        limit: 20,
        offset: 0,
        q: nil
      }
  """
  def add_filter(query, filter) do
    %{query | filters: query.filters ++ [filter]}
  end

  @doc """
  Adds sorting criteria to the query.

  This function allows the user to specify sorting options for the query results.

  ## Parameters
    - `query` (Meilisearch.Query): The current query struct.
    - `sorts` (list): A list of fields to sort the query results by.

  ## Returns
    - `%Meilisearch.Query{}`: The updated query struct with the new sorting criteria.

  ## Example
      iex> query = Meilisearch.Query.from("products")
      ...> Meilisearch.Query.order_by(query, [{"price", "desc"}])
      %Meilisearch.Query{index: "products", filters: [], sort: [{"price", "desc"}], limit: 20, offset: 0, q: nil}
  """
  def order_by(query, sorts) when is_list(sorts) do
    %{query | sort: query.sort ++ sorts}
  end

  def order_by(query, {_field, _order} = sort) do
    %{query | sort: query.sort ++ [sort]}
  end

  @doc """
  Sets the maximum number of results to return.

  This function limits the number of results returned by the query.

  ## Parameters
    - `query` (Meilisearch.Query): The current query struct.
    - `limit` (integer): The maximum number of results to return.

  ## Returns
    - `%Meilisearch.Query{}`: The updated query struct with the new limit.

  ## Example
      iex> query = Meilisearch.Query.from("products")
      ...> Meilisearch.Query.limit(query, 10)
      %Meilisearch.Query{index: "products", filters: [], sort: [], limit: 10, offset: 0, q: nil}
  """
  def limit(query, limit) when is_integer(limit) and limit > 0 do
    %{query | limit: limit}
  end

  @doc """
  Sets the offset for pagination.

  This function defines the offset (pagination starting point) for the query.

  ## Parameters
    - `query` (Meilisearch.Query): The current query struct.
    - `offset` (integer): The starting offset for pagination.

  ## Returns
    - `%Meilisearch.Query{}`: The updated query struct with the new offset.

  ## Example
      iex> query = Meilisearch.Query.from("products")
      ...> Meilisearch.Query.offset(query, 10)
      %Meilisearch.Query{index: "products", filters: [], sort: [], limit: 20, offset: 10, q: nil}
  """
  def offset(query, offset) when is_integer(offset) and offset >= 0 do
    %{query | offset: offset}
  end

  @doc """
  Sets the search term for the query.

  This function allows the user to specify the search term that will be used in the query.

  ## Parameters
    - `query` (Meilisearch.Query): The current query struct.
    - `search_term` (string): The search term.

  ## Returns
    - `%Meilisearch.Query{}`: The updated query struct with the new search term.

  ## Example
      iex> query = Meilisearch.Query.from("products")
      ...> Meilisearch.Query.search(query, "laptop")
      %Meilisearch.Query{index: "products", filters: [], sort: [], limit: 20, offset: 0, q: "laptop"}
  """
  def search(query, search_term) when is_binary(search_term) do
    %{query | q: search_term}
  end
end
