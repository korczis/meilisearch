defmodule Meilisearch do
  @moduledoc """
  Meilisearch library / SDK / interface entry-point
  """

  defmacro __using__(_opts \\ []) do
    quote do
      alias Meilisearch.Client
    end
  end
end
